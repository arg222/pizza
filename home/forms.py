from django import forms
from pizza.models import Pizza


class PizzaForm(forms.ModelForm):
    class Meta:
        model = Pizza
        fields = ('name', 'dough_type', 'company', 'image')


class SearchForm(forms.Form):
    search_query = forms.CharField(max_length=150, label='Search for pizzas')
