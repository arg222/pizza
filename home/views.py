from django.shortcuts import render, redirect
from pizza.models import Pizza
from .forms import PizzaForm, SearchForm


def home(request):
    pizza_list = Pizza.objects.all()
    return render(request, 'home/index.html', {'pizza_list': pizza_list})


def add_pizza(request):
    forms = PizzaForm(request.POST, request.FILES)
    if forms.is_valid():
        print(forms.cleaned_data)
        forms.save()
        return redirect('home:home')
    return render(request, 'home/add_pizza.html', context={'forms': forms})


def search(request):
    pizzas = []
    form = SearchForm(request.GET)
    if form.is_valid():
        search_query = form.cleaned_data['search_query']
        pizzas = Pizza.objects.filter(name__icontains=search_query)
    return render(request, 'home/search.html', context={'form': form, 'pizzas': pizzas})
