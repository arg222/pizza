from django.urls import path
from home import views

app_name = 'pizza'
urlpatterns = [
    path('', views.home, name='home'),
    path('add_pizza/', views.add_pizza, name='add_pizza'),
    path('search/', views.search, name='search'),
]
